# My Thoughts - Laravel Vue Crud Demo App


Una vez que tenga Docker y Docker Compose instalados, puede poner en funcionamiento este entorno con:

```
docker-compose build app
docker-compose up -d
```

Posteriormente para instalar dependencias y para generar la llave puede hacer 

```
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
```

si todo sale bien la aplicacion deberia estar ejecutandose en :

```
http://localhost:9000/
```